<?php include("includes/nav.php"); ?>


<!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">



        <!--end:: Widgets/Stats-->

        <!--Begin::Section-->
        <div class="row">

      <?php


                                        $query = "SELECT * FROM books";
                                        $result = $db->query($query);

                                        /* associative array */
                                        if ($result->num_rows > 0) {
                                        while ($row = mysqli_fetch_assoc($result)) {

                                            ?>


            <div class="col-xl-3">





                <div class="kt-portlet kt-portlet--height-fluid kt-widget19">
                    <div class="kt-portlet__body kt-portlet__body--fit kt-portlet__body--unfill">
                        <div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides" style="min-height: 300px; background-image: url(./assets/media//products/product4.jpg)">

                            <div class="kt-widget19__shadow"></div>
                            <div class="kt-widget19__labels">
                                <a href="#" class="btn btn-label-light-o2 btn-bold ">Recent</a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget19__wrapper">
                            <div class="kt-widget19__content">
                                <div class="kt-widget19__userpic">
                                    <img src="./assets/media//users/user1.jpg" alt="">
                                </div>
                                <div class="kt-widget19__info">
                                    <a href="#" class="kt-widget19__username">
                                       <?php echo $book['book_name']; ?>
                                    </a>
                                    <span class="kt-widget19__time">
															<?php echo $book['book_author']; ?>
														</span>
                                </div>
                                <div class="kt-widget19__stats">
														<span class="kt-widget19__number kt-font-brand">
															<?php echo $book['book_qty']; ?>
														</span>
                                    <a href="#" class="kt-widget19__comment">
                                        Available books
                                    </a>
                                </div>
                            </div>
                            <div class="kt-widget19__text">
                                <?php echo $book['book_description']; ?>
                            </div>
                        </div>
                        <div class="kt-widget19__action">
                            <a href="<?php echo USER_PATH; ?>book_reserve.php?id=<?php echo $book['book_id']; ?>" class="btn btn-sm btn-label-brand btn-bold">Reserve</a>
                        </div>
                    </div>
                </div>


                <!--end:: Widgets/Blog-->
            </div>

             <?php } } ?>



<?php include("includes/footer.php"); ?>