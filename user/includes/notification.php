<?php
session_start();
define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"] . "/");

include(ROOT_PATH . "init.php");

if (isset($_POST["view"])) {
    $sql = $db->query("SELECT * FROM notifications ORDER BY n_id DESC limit 5");
    $output = '';
    if ($sql->num_rows > 0) {
        while ($row = mysqli_fetch_assoc($sql)) {
            $output .= '
             <a>.$row["notification"].</a>
            ';
        }
    } else
    {
        $output .= '
         <a>.$row["notification"].</a>
        ';
    }
    $sql1 = $db->query("SELECT * FROM notifications WHERE is_read = 1 AND type = 2");
    $count = $sql1->num_rows;
    $data = array(
        'notification'          => $output,
        'unseen_notification'   => $count
    );
    echo json_encode($data);

}