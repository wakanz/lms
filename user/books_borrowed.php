<?php


include "includes/nav.php";

?>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Issued Books / E-Books
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">

                            &nbsp;
                            <a href="<?php echo USER_PATH; ?>index.php"
                               class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-plus"></i>Request New Book
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                        <ul class="nav nav-pills nav-fill" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#books">Issued Books</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#ebooks">Issued E-Books</a>
                            </li>
                        </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="books" role="tabpanel">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                <thead>
                                <tr>
                                    <th>Book Name</th>
                                    <th>Issue Date</th>
                                    <th>Return Date</th>
                                    <th>Fine</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php


                                if (isset($_SESSION['name'])) {

                                    $user_id = $_SESSION['id'];

                                    $query = "SELECT borrow.borrow_id, borrow.date_out, borrow.due_date, borrow.status, books.book_name, fines.fine_amnt FROM borrow INNER JOIN users ON  borrow.user_id = users.user_id INNER JOIN  books ON  borrow.book_id = books.book_id INNER JOIN fines ON fines.borrow_id = borrow.borrow_id WHERE borrow.user_id = '$user_id' AND borrow.book_type = 1 ORDER BY 'borrow' DESC ";

                                    $result = $db->query($query);

                                    /* associative array */
                                    if ($result->num_rows > 0) {
                                        while ($row = mysqli_fetch_assoc($result)) {

                                            $bo_id = $row['borrow_id'];
                                            $d = strtotime($row['due_date']);
                                            $c = strtotime(date("Y-m-d"));
                                            //$amount =  $row['fine'];
                                            $diff = $c -$d;
                                            $diff = $diff/(60*60*24);

                                            $diff =  $diff * 10;
                                            if ($diff >= 0) {
                                                $query = $db->query("UPDATE fines SET fine_amnt = '$diff' WHERE borrow_id = '$bo_id'");
                                            }


                                            ?>

                                            <tr>
                                                <td value><?php echo $row['book_name']; ?></td>
                                                <td><?php echo $row['date_out']; ?></td>b
                                                <td><?php echo $row['due_date']; ?></td>
                                                <td><?php echo $row['fine_amnt']; ?></td>
                                                <td>
                                                    <?php if ($row['status'] == 1) { ?>
                                                        <a class="btn btn-warning btn-sm"></i>Return Book</a>
                                                    <?php } elseif ($row['status'] == 0) { ?>
                                                        <button type="button" class="btn btn-success btn-sm">Returned</button>
                                                    <?php } ?>
                                                </td>

                                            </tr>
                                        <?php }
                                    }
                                } ?>

                                </tbody>
                            </table>

                            <!--end: Datatable -->
                        </div>
                        <div class="tab-pane" id="ebooks" role="tabpanel">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                <thead>
                                <tr>
                                    <th>Book Name</th>
                                    <th>Issue Date</th>
                                    <th>Return Date</th>
                                    <th>Fine</th>
                                    <th>Status</th>
                                    <th>Download Link</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                if (isset($_SESSION['name'])) {

                                    $user_id = $_SESSION['id'];

                                    $query = "SELECT borrow.borrow_id, borrow.date_out, borrow.due_date, borrow.status, ebooks.book_name, fines.fine_amnt FROM borrow INNER JOIN users ON  borrow.user_id = users.user_id INNER JOIN  ebooks ON  borrow.book_id = ebooks.book_id INNER JOIN fines ON fines.borrow_id = borrow.borrow_id WHERE borrow.user_id = '$user_id' AND borrow.book_type = 2 ORDER BY 'borrow' DESC ";

                                    $result = $db->query($query);

                                    /* associative array */
                                    if ($result->num_rows > 0) {
                                        while ($row = mysqli_fetch_assoc($result)) {

                                            ?>

                                            <tr>
                                                <td value><?php echo $row['book_name']; ?></td>
                                                <td><?php echo $row['date_out']; ?></td>
                                                <td><?php echo $row['due_date']; ?></td>
                                                <td><?php $row['fine_amnt']; ?></td>
                                                <td>
                                                    <?php if ($row['status'] == 1) { ?>
                                                        <a class="btn btn-primary btn-sm" href="<?php echo BASE_URL; ?>assets/upload/ebook/<?php echo $row['ebook_file']; ?>">Download</a>
                                                    <?php } elseif ($row['status'] == 0) { ?>

                                                    <?php } ?>
                                                </td>

                                            </tr>
                                        <?php }
                                    }
                                } ?>

                                </tbody>
                            </table>

                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


<?php include("includes/footer.php"); ?>