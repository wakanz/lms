<?php include("includes/nav.php"); ?>

    <!-- begin:: Content -->
    <div class="book-container">


        <form class="kt-form kt-form--label-right">
            <div class="kt-portlet__body">
                <div class="form-group ">
                    <div class="kt-input-icon kt-input-icon--right">
                        <div class="input-group">
                            <input type="text" name="search" class="form-control"
                                   placeholder="Search by E-book name or ISBN..." id="generalSearch">
                            <div class="input-group-append">
                                <button name="search" class="btn btn-secondary" type="button"><i class="la la-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="kt-subheader__main">
            <h3 class="l-book">LATEST e-BOOKS</h3>
        </div>
        <!--end:: Widgets/Stats-->

        <!--Begin::Section-->
        <div class="row">

            <?php
            $query = "SELECT * FROM ebooks";
            $result = $db->query($query);

            /* associative array */
            if ($result->num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {

                    ?>

                    <div class="col-xl-4">
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <a href="<?php echo USER_PATH; ?>ebook_view.php?id=<?php echo $row['book_id']; ?>">
                                        <img class="kt-widget__img kt-hidden-"
                                             src="<?php echo BASE_URL; ?>assets/upload/cover/<?php echo $row["ebook_image"]; ?>"
                                             alt="image">
                                    </a>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="<?php echo USER_PATH; ?>ebook_view.php?id=<?php echo $row['book_id']; ?>"
                                           class="kt-widget__username">
                                            <?php echo $row['book_name']; ?>
                                        </a>
                                        <p style="text-align: center;">By: <?php echo $row['book_author']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php }
            } ?>

        </div>
    </div>


<?php include("includes/footer.php"); ?>