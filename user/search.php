<?php

include "includes/nav.php";


?>


<!-- begin:: Content -->
<div class="book-container">

    <div class="kt-subheader__main">
        <h3 class="l-book">SEARCHED BOOKS</h3>
    </div>
    <!--end:: Widgets/Stats-->

    <!--Begin::Section-->
    <div class="row">

        <?php
        if (isset($_POST['search']))
        {
        $s = $_POST['search'];
        $q = $db->query("SELECT * FROM books WHERE book_name like '%$s%'");

            /* associative array */
           if ($q->num_rows == 0) {
               echo "Sorry No Books found.";
           }else{
                while ($row = mysqli_fetch_assoc($q)) {

                    echo $row['book_id'];
                    ?>

                    <div class="col-lg-4">
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <a href="<?php echo USER_PATH; ?>book_view.php?id=<?php echo $row['book_id']; ?>">
                                        <img class="kt-widget__img kt-hidden-"
                                             src="<?php echo BASE_URL; ?>assets/upload/cover/<?php echo $row["book_image"]; ?>"
                                             alt="image">
                                    </a>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <a href="<?php echo USER_PATH; ?>book_view.php?id=<?php echo $row['book_id']; ?>"
                                           class="kt-widget__username">
                                            <?php echo $row['book_name']; ?>
                                        </a>
                                        <p style="text-align: center;">By: <?php echo $row['book_author']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php }

        }}?>

    </div>
</div>



