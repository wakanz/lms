
<?php
include("includes/nav.php");

include(ROOT_PATH . "init.php");


?>


<!-- begin:: Content -->
<div class="kt-container book-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">


    <!--end:: Widgets/Stats-->

    <!--Begin::Section-->
    <div class="row">

        <?php
        $id=$_GET['id'];        // Collecting data from query string
        if(!is_numeric($id)){ // Checking data it is a number or not
            echo "Data Error";
            exit;
        }

        $query = $db->query("select * from ebooks where book_id= '$id'");

        /* associative array */
        if ($query->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($query)) {

                ?>


                <div class="col-xl-8" style="margin: 0px auto">

                    <div class="kt-portlet kt-portlet--height-fluid">
                        <div class="kt-portlet__body">
                            <div class="kt-widget kt-widget--user-profile-3">
                                <div class="kt-widget__top">
                                    <div class="kt-widget19__userpic" style="transform: translateZ(0px); left: 370px; top: 354.5px; height: 467px; width: 418px;">
                                        <img style='z-index: 999; overflow: hidden; margin-left: 0px; margin-top: 0px; background-position: 0px 0px; width: 318px; float: left; cursor: crosshair; background-repeat: no-repeat; position: absolute; top: 0px; left: 0px; display: block; background-size: 418px 467px;' src="<?php echo BASE_URL;?>assets/upload/cover/<?php echo $row["ebook_image"]; ?>" alt="image">
                                    </div>
                                    <div class="kt-widget__content">
                                        <div class="kt-widget__head">
                                            <a href="<?php echo USER_PATH; ?>book-view.php?id=<?php echo $row['book_id']; ?>" class="kt-widget__username">
                                                <?php echo $row['book_name']; ?>
                                            </a>

                                        </div>
                                        <div class="kt-widget__subhead">
                                            <a href="#">by: <?php echo $row['book_author']; ?></a>
                                        </div>
                                        <div class="kt-widget__info">
                                            <div class="kt-widget__desc">
                                                <?php echo substr($row['book_description'], 0, 160); ?>
                                            </div>

                                        </div>
                                        <a>
                                        <iframe src="https://docs.google.com/gview?url=<?php echo URL; ?>assets/upload/ebook/<?php echo $row["ebook_file"]; ?>&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>Read E-Book</a>
                                    </div>
                                </div>

                            </div>
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">
                                    <ul class="nav nav-tabs  nav-tabs-line" role="tablist">

                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#kt_tabs_1_3" role="tab" aria-selected="true" style="font-size: 1.3rem; font-weight: 500;">E-BOOK DESCRIPTION</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">

                                        <div class="tab-pane active" id="kt_tabs_1_3" role="tabpanel">

                                            <p style="font-size: 1.2rem"> <?php echo $row['book_description']; ?> </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <!--end:: Widgets/Blog-->
                </div>

            <?php }
        } ?>
    </div>

    <?php include("includes/footer.php"); ?>
