<?php

include "includes/nav.php";

$user_id = $_SESSION['id'];
?>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Reserved Books
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">

                            &nbsp;
                            <a href="<?php echo USER_PATH; ?>index.php"
                               class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-plus"></i>Reserve New Book
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <ul class="nav nav-pills nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_tabs_5_1">Books Reserved</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_tabs_5_2">E-Books Reserved</a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_tabs_5_1" role="tabpanel">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                <thead>
                                <tr>
                                    <th>Book Name</th>
                                    <th>Date Requested</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                // $query = "SELECT * FROM reserved";
                                $query = "SELECT  reserved.reserve_id, reserved.r_date, reserved.status, books.book_name  FROM reserved, books WHERE reserved.book_id = books.book_id AND reserved.user_id = '$user_id' AND reserved.book_type = 1";



                                $result = $db->query($query);

                                /* associative array */
                                if ($result->num_rows > 0) {
                                    while ($row = mysqli_fetch_assoc($result)) {

                                        ?>

                                        <tr>
                                            <td class="col-8"><?php echo $row['book_name']; ?></td>
                                            <td><?php echo $row['r_date']; ?></td>
                                            <td>
                                                <?php if($row['status'] == 1){ ?>
                                                    <button type="button" class="btn btn-success btn-sm">Accepted</button>
                                                <?php }elseif ($row['status'] == 0) { ?>
                                                    <button type="button" class="btn btn-warning btn-sm">Pending</button>
                                                <?php } ?>
                                            </td>


                                            <td><span class="dropdown">
                            <a href="<?php echo USER_PATH; ?>php/book_delete.php?id=<?php echo $row['reserve_id']; ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                              <i class="la la-trash"></i>
                            </a>

                        </span>
                                            </td>


                                        </tr>
                                    <?php }
                                } ?>

                                </tbody>
                            </table>

                            <!--end: Datatable -->
                        </div>

                        <div class="tab-pane" id="kt_tabs_5_2" role="tabpanel">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                <thead>
                                <tr>
                                    <th>Book Name</th>
                                    <th>Date Requested</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                // $query = "SELECT * FROM reserved";
                                $query = "SELECT  reserved.reserve_id, reserved.r_date, reserved.status, ebooks.book_name  FROM reserved, ebooks WHERE reserved.book_id = ebooks.book_id AND reserved.user_id = '$user_id' AND reserved.book_type = 2";



                                $result = $db->query($query);

                                /* associative array */
                                if ($result->num_rows > 0) {
                                    while ($row = mysqli_fetch_assoc($result)) {

                                        ?>

                                        <tr>
                                            <td class="col-8"><?php echo $row['book_name']; ?></td>
                                            <td><?php echo $row['r_date']; ?></td>
                                            <td>
                                                <?php if($row['status'] == 1){ ?>
                                                    <button type="button" class="btn btn-success btn-sm">Accepted</button>
                                                <?php }elseif ($row['status'] == 0) { ?>
                                                    <button type="button" class="btn btn-warning btn-sm">Pending</button>
                                                <?php } ?>
                                            </td>


                                            <td><span class="dropdown">
                            <a href="<?php echo USER_PATH; ?>php/ebook_delete.php?id=<?php echo $row['reserve_id']; ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                              <i class="la la-trash"></i>
                            </a>

                        </span>
                                            </td>


                                        </tr>
                                    <?php }
                                } ?>

                                </tbody>
                            </table>

                            <!--end: Datatable -->
                        </div>

                    </div>
                </div>
            </div>

            <!--end::Portlet-->


            <div class="kt-portlet__body">


            </div>
        </div>
    </div>


<?php include("includes/footer.php"); ?>