<?php

include "includes/nav.php";

?>


<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-8" style="margin: 0 auto;">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Return Book
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" method="post" action="<?php echo ADMIN_PATH; ?>php/book_return.php">
                    <div class="kt-portlet__body">
                        <?php

                        $id = $_GET['id'];
                        //$query = "SELECT borrow.borrow_id, books.book_name, users.user_id, users.username, fines.fine_amnt  FROM borrow, books, users, fines WHERE borrow.borrow_id = '$id'";
                        $query = "SELECT borrow.borrow_id, borrow.due_date, users.username, books.book_name FROM borrow, books, users WHERE borrow_id = '$id' AND books.book_id = borrow.book_id AND users.user_id = borrow.user_id";
                        $result = $db->query($query);

                        /* associative array */
                        if ($result->num_rows > 0) {
                        while ($row = mysqli_fetch_assoc($result)) {

                        $bo_id = $row['borrow_id'];
                        $d = strtotime($row['due_date']);
                        $c = strtotime(date("Y-m-d"));
                        //$amount =  $row['fine'];
                        $diff = $c -$d;
                        $diff = $diff/(60*60*24);

                        $diff =  $diff * 10;

                        ?>
                        <div class="form-group">
                            <input type="hidden" name="r_id" value="<?php echo $row['borrow_id']; ?>">
                            <input type="hidden" name="b_id" value="<?php echo $row['book_id']; ?>">
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Book Name </label>
                                <input type="text" class="form-control" disabled="disabled" value="<?php echo $row['book_name']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Fine Amount </label>
                                <input name="fine" type="text" class="form-control" disabled="disabled" value="<?php echo $diff ?>">
                            </div>
                            <input type="hidden" name="u_id" value="<?php echo $row['user_id']; ?>">
                            <div class="form-group">
                                <label for="username" class="form-control-label">Requested By </label>
                                <input type="text" name="u_id" class="form-control" disabled="disabled" value="<?php echo $row['username']; ?>">
                            </div>
                            <div class="form-group">
                                <label>Return Date</label>

                                <input type="text" class="form-control" placeholder="Date In" name="d_in"  disabled="disabled"value="<?php echo date('Y-m-d') ?>" />

                            </div>

                            <?php } }?>


                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-2">
                                    </div>
                                    <div class="col-10">
                                        <button name="confirm" class="btn btn-success">Submit</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                </form>



            </div>

        </div>
    </div>
</div>


<?php

include "includes/footer.php";

?>

