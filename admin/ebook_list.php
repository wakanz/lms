<?php

include "includes/nav.php";

?>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Ebook List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">                            &nbsp;
                            <a href="<?php echo ADMIN_PATH; ?>ebook_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-plus"></i>Add New Ebook
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                    <tr>
                        <th>Ebook Name</th>
                        <th>Ebook ISBN</th>
                        <th>Ebook Description</th>
                        <th>Ebook Image</th>
                        <th>Ebook file</th>
                        <th>Ebook Author</th>
                        <th>Ebook Section</th>
                        <th>Ebook Publish Date</th>
                        <th>Ebook Location</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

$query = "SELECT * FROM ebooks";
$result = $db->query($query);

/* associative array */
if ($result->num_rows > 0) {
    while ($row = mysqli_fetch_assoc($result)) {

        ?>

                        <tr>
                            <td><?php echo $row['book_name']; ?></td>
                            <td><?php echo $row['book_isbn']; ?></td>
                            <td><?php echo $row['book_description']; ?></td>
                            <td><?php echo '<img id="my" height="150" width="150"src="assets/upload/cover/'.$row["ebook_image"].'" />'; ?></td>
                            <td><a href="<?php echo BASE_URL; ?>assets/upload/ebook/<?php echo $row['ebook_file']; ?>">Download</a></td>
                            <td><?php echo $row['book_author']; ?></td>
                            <td><?php echo $row['book_section']; ?></td>
                            <td><?php echo $row['publish_date']; ?></td>
                            <td><?php echo $row['book_location']; ?></td>

                            <td><span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="<?php echo ADMIN_PATH; ?>ebook_edit.php?id=<?php echo $row['book_id']; ?>"><i class="la la-edit"></i> Edit</a>
                                <a class="dropdown-item" name="del" href="<?php echo ADMIN_PATH; ?>php/ebook_delete.php?id=<?php echo $row['book_id']; ?>"><i class="la la-leaf"></i> Delete</a>
                            </div>
                        </span>

                        </tr>
                    <?php } } ?>
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>

<?php

include "includes/footer.php";

?>