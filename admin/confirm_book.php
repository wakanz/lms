<?php

include "includes/nav.php";

?>


<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-8" style="margin: 0 auto;">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Confirm Book
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" method="post" action="<?php echo ADMIN_PATH; ?>php/books_reserved.php">
                    <div class="kt-portlet__body">
                        <?php

                        $id = $_GET['id'];
                        $query = "SELECT reserved.reserve_id, reserved.book_id, books.book_name, users.user_id, users.username  FROM reserved, books, users WHERE reserved.reserve_id = '$id' AND reserved.book_id = books.book_id AND reserved.user_id = users.user_id";
                        $result = $db->query($query);

                        /* associative array */
                        if ($result->num_rows > 0) {
                            while ($row = mysqli_fetch_assoc($result)) {

                                ?>
                        <div class="form-group">
                            <input type="hidden" name="r_id" value="<?php echo $row['reserve_id']; ?>">
                            <input type="hidden" name="b_id" value="<?php echo $row['book_id']; ?>">
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Book Name </label>
                                <input type="text" class="form-control" disabled="disabled" value="<?php echo $row['book_name']; ?>">
                            </div>
                                <input type="hidden" name="u_id" value="<?php echo $row['user_id']; ?>">
                        <div class="form-group">
                            <label for="username" class="form-control-label">Requested By </label>
                            <input type="text" name="u_id" class="form-control" disabled="disabled" value="<?php echo $row['username']; ?>">
                        </div>
                                <div class="form-group">
                                    <label>Due Date</label>

                                        <input type="text" class="form-control" id="kt_datepicker_1" readonly placeholder="Select return date" name="r_date" />

                                </div>

                        <?php } }?>


                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-2">
                                </div>
                                <div class="col-10">
                                    <button name="confirm" class="btn btn-success">Submit</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>



            </div>

        </div>
    </div>
</div>


<?php

include "includes/footer.php";

?>

