<?php
define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"] . "/");

include(ROOT_PATH . "init.php");

if(isset($_POST['submit']))
{

    $book_name            =           $_POST['name'];
    $book_isbn            =           $_POST['isbn'];
    $book_description     =           $_POST['desc'];
    $publish_date         =           date('Y-m-d', strtotime(str_replace('-', '/', $_POST['p_date'])));
    $book_qty             =           $_POST['qty'];
    $book_author          =           $_POST['author'];
    $book_section         =           $_POST['section'];
    $book_location        =           $_POST['loc'];
    $target = ROOT_PATH ."assets/upload/cover/";
    $imgname = basename($_FILES["image"]["name"]);
    $newname = $target.basename($_FILES["image"]["name"]);
    $img_type = pathinfo($newname,PATHINFO_EXTENSION);
    if($img_type!='jpg' && $img_type!='JPG' && $img_type!='png' && $img_type!='PNG'){
        $message="Please ensure you are entering an image";
    }else{
        if($book_type != 'pdf'){
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $newname)) {
                    $sql = $db->query("INSERT INTO books (book_name,book_isbn,book_description,publish_date,book_qty,book_image,book_author,book_section,book_location) VALUES ('$book_name','$book_isbn','$book_description','$publish_date','$book_qty','$imgname','$book_author','$book_section','$book_location')");

                    if($sql){
                        $message = "upload successful";
                    }else{
                        $message = "upload failed1";
                    }
            }else{
                $message = "upload failed";
            }

        }
    }
}
else{
    $message="";
    $title="";
}


header('location: ../book_list.php');


//die(mysqli_error($db));

$db->close();
