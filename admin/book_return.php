<?php

include "includes/nav.php";

?>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Book List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="la la-download"></i> Export
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__section kt-nav__section--first">
                                            <span class="kt-nav__section-text">Choose an option</span>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-print"></i>
                                                <span class="kt-nav__link-text">Print</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-copy"></i>
                                                <span class="kt-nav__link-text">Copy</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                <span class="kt-nav__link-text">Excel</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                <span class="kt-nav__link-text">CSV</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                                <span class="kt-nav__link-text">PDF</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            &nbsp;
                            <a href="<?php echo ADMIN_PATH; ?>book_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-plus"></i>Add New Book
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                    <tr>
                        <th>Book Name</th>
                        <th>User Name</th>
                        <th>Date Borrowed</th>
                        <th>Due date</th>
                        <th>Fine</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    $query = "SELECT borrow.borrow_id, borrow.date_out, borrow.due_date, borrow.status, users.username,books.book_name, fines.fine_amnt FROM borrow, users,books, fines WHERE borrow.user_id = users.user_id AND borrow.book_id = books.book_id AND  fines.borrow_id = borrow.borrow_id AND borrow.status = 1";


                    $result = $db->query($query);

                    /* associative array */
                    if ($result->num_rows > 0) {
                        while ($row = mysqli_fetch_assoc($result)) {
                            $bo_id = $row['borrow_id'];
                            $d = strtotime($row['due_date']);
                            $c = strtotime(date("Y-m-d"));
                            //$amount =  $row['fine'];
                            $diff = $c -$d;
                            $diff = $diff/(60*60*24);

                            $diff =  $diff * 10;
                            if ($diff >= 0) {
                                $query = $db->query("UPDATE fines SET fine_amnt = '$diff' WHERE borrow_id = '$bo_id'");
                            }
                            ?>

                            <tr>
                                <td value><?php echo $row['book_name']; ?></td>
                                <td><?php echo $row['username']; ?></td>
                                <td><?php echo $row['date_out']; ?></td>
                                <td><?php echo $row['due_date']; ?></td>
                                <td><?php echo $row['fine_amnt']; ?></td>

                                <td>
                                    <?php if($row['status'] == 1)
                                    {?>
                                        <a class="btn btn-primary btn-sm" href="<?php echo ADMIN_PATH; ?>return.php?id=<?php echo $row['borrow_id']; ?>"></i>
                                        Return Book</a>
                                   <?php
                                    }elseif ($row['status'] == 0) { ?>
                                        <button type="button" class="btn btn-success btn-sm">Returned</button>
                                    <?php } ?>

                                </td>

                            </tr>
                        <?php } } ?>

                        </tbody>
                        </table>

                        <!--end: Datatable -->
                        </div>
                        </div>
                        </div>


                        <?php

include "includes/footer.php";

?>