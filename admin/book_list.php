<?php

include "includes/nav.php";

?>
<!-- begin:: Content -->
						<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
Book List
</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="la la-download"></i> Export
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<ul class="kt-nav">
															<li class="kt-nav__section kt-nav__section--first">
																<span class="kt-nav__section-text">Choose an option</span>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-print"></i>
																	<span class="kt-nav__link-text">Print</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-copy"></i>
																	<span class="kt-nav__link-text">Copy</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-excel-o"></i>
																	<span class="kt-nav__link-text">Excel</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-text-o"></i>
																	<span class="kt-nav__link-text">CSV</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-pdf-o"></i>
																	<span class="kt-nav__link-text">PDF</span>
																</a>
															</li>
														</ul>
													</div>
												</div>
												&nbsp;
												<a href="<?php echo ADMIN_PATH; ?>book_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>Add New Book
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>Book Image</th>
												<th>Book Name</th>
												<th>Book ISBN</th>
												<th>Book Description</th>
												<th>Book Quantity</th>
												<th>Book Author</th>
												<th>Book Section</th>
												<th>Book Publish Date</th>
												<th>Book Location</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
                                        <?php


                                        $query = "SELECT * FROM books";
                                        $result = $db->query($query);

                                        /* associative array */
                                        if ($result->num_rows > 0) {
                                        while ($row = mysqli_fetch_assoc($result)) {

                                            ?>

											<tr>
                                                <td><?php echo '<img id="my" height="150" width="150"src="assets/upload/cover/'.$row["book_image"].'" />'; ?></td>
												<td><?php echo $row['book_name']; ?></td>
												<td><?php echo $row['book_isbn']; ?></td>
												<td><?php echo $row['book_description']; ?></td>
												<td><?php echo $row['book_qty']; ?></td>
												<td><?php echo $row['book_author']; ?></td>
												<td><?php echo $row['book_section']; ?></td>
												<td><?php echo $row['publish_date']; ?></td>
												<td><?php echo $row['book_location']; ?></td>
												<td><span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="<?php echo ADMIN_PATH; ?>book_edit.php?id=<?php echo $row['book_id']; ?>"><i class="la la-edit"></i> Edit</a>
                                <a class="dropdown-item" name="del" href="<?php echo ADMIN_PATH; ?>php/book_delete.php?id=<?php echo $row['book_id']; ?>"><i class="la la-leaf"></i> Delete</a>
                            </div>
                        </span>
                                                </td>

											</tr>
    <?php } } ?>
                                       
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
						</div>

<?php

include "includes/footer.php";

?>