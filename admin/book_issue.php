<?php

include "includes/nav.php";


?>




<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-8" style="margin: 0 auto;">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Issue Book
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" method="post" action="<?php echo ADMIN_PATH; ?>php/book_borrow.php">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label class="col-form-label col-2">Book Name / ISBN</label>
                            <div class=" col-lg-10">

                                <select class="form-control kt-select2" id="kt_select2_4" placeholder="Select Book" name="books">
                                    <option></option>
                                    <?php


                                    $query = "SELECT * FROM books";
                                    $result = $db->query($query);



                                    /* associative array */
                                    if ($result->num_rows > 0) {
                                    while ($row = mysqli_fetch_assoc($result)) {

                                        ?>

                                        <option value="<?php echo $row['book_id']?>"><?php echo $row['book_name'] ;?></option>
                                        <?php
                                    } }
                                    ?>
                                </select>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-2">User</label>
                            <div class=" col-lg-10">

                                <select class="form-control kt-select2" id="kt_select2_20" placeholder="Select Book" name="users">
                                    <option></option>
                                    <?php


                                    $query = "SELECT * FROM users";
                                    $result = $db->query($query);



                                    /* associative array */
                                    if ($result->num_rows > 0) {
                                    while ($row = mysqli_fetch_assoc($result)) {

                                        ?>



                                        <option value="<?php echo $row['user_id']?>"><?php echo $row['username'] ;?></option>
                                        <?php
                                    } }
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2">Return Date</label>
                            <div class="col-10">
                                <input type="text" class="form-control" id="kt_datepicker_1" readonly placeholder="Select return date" name="r_date" />
                            </div>
                        </div>



                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-2">
                                </div>
                                <div class="col-10">
                                    <button name="submit" class="btn btn-success">Submit</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>



            </div>

        </div>
    </div>

</div>

<?php


include "includes/footer.php";

?>

