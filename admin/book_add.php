<?php

include "includes/nav.php";

?>


<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-8" style="margin: 0 auto;">

<!--begin::Portlet-->
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Add New Book
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form kt-form--label-right" method="post" action="<?php echo ADMIN_PATH; ?>php/book_add.php" enctype="multipart/form-data">
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label for="book_name" class="col-2 col-form-label">Book Name</label>
                <div class="col-10">
                    <input class="form-control" type="text" placeholder="Enter Book Name" name="name">
                </div>
            </div>
            <div class="form-group row">
                <label for="book_name" class="col-2 col-form-label">Book ISBN</label>
                <div class="col-10">
                    <input class="form-control" type="text" placeholder="Enter Book ISBN" name="isbn">
                </div>
            </div>
            <div class="form-group row">
                <label for="book_name" class="col-2 col-form-label">Book Description</label>
                <div class="col-10">
                    <input class="form-control" type="text" placeholder="Enter Book Description" name="desc">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2">Publish Date</label>
                <div class="col-10">
                    <input type="text" class="form-control" id="kt_datepicker_1" readonly placeholder="Select publish date" name="p_date" />
                </div>
            </div>
            <div class="form-group row">
                <label for="book_name" class="col-2 col-form-label">Number of Copies</label>
                <div class="col-10">
                    <input class="form-control" type="text" placeholder="Enter Number of Copies" name="qty">
                </div>
            </div>
            <div class="form-group row">
                <label for="book_name" class="col-2 col-form-label">Book Author</label>
                <div class="col-10">
                    <input class="form-control" type="text" placeholder="Enter Book Author" name="author">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-2">Book Cover Image</label>
                <div class="col-10">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image">
                        <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="book_name" class="col-2 col-form-label">Book Section</label>
                <div class="col-10">
                    <input class="form-control" type="text" placeholder="Enter Book Name" name="section">
                </div>
            </div>
            <div class="form-group row">
                <label for="book_name" class="col-2 col-form-label">Book Location</label>
                <div class="col-10">
                    <input class="form-control" type="text" placeholder="Enter Book Name" name="loc">
                </div>
            </div>


        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button name="submit" class="btn btn-success">Submit</button>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

        </div>
    </div>
</div>


<?php

include "includes/footer.php";

?>

