<?php

include "includes/nav.php";

?>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        Pay Fines
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                   <thead>
                    <tr>

                        <th>Book Name</th>
                        <th>User Name</th>
                        <th>Issue Date</th>
                        <th>Return date</th>
                        <th>Fines</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>


                    <?php
                    // $query = "SELECT * FROM borrow WHERE status = 1";
                    $query = "SELECT borrow.date_out, borrow.due_date, borrow.date_in, borrow.status, users.username, books.book_name, fines.fine_amnt, fines.fine_id FROM borrow, users,books, fines WHERE borrow.user_id = users.user_id AND borrow.book_id = books.book_id AND  fines.borrow_id = borrow.borrow_id AND fines.status = 1";


                    $result = $db->query($query);

                    /* associative array */
                    if ($result->num_rows > 0) {
                        while ($row = mysqli_fetch_assoc($result)) {

                            ?>

                            <tr>

                                <td value><?php echo $row['book_name']; ?></td>
                                <td value><?php echo $row['username']; ?></td>
                                <td><?php echo $row['date_out']; ?></td>
                                <td><?php echo $row['date_in']; ?></td>

                                <td><?php echo $row['fine_amnt']; ?></td>

                                <td>


                                <td>
                                    <button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_5">Pay Fine</button>
                                </td>




                            </tr>

                    <!--begin::Modal-->
                    <div class="modal fade" id="kt_modal_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Pay Fine</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Fine </label>
                                            <input type="email" class="form-control" disabled="disabled" value="<?php echo $row['fine_amnt']; ?>">
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-primary btn-sm" href="<?php echo ADMIN_PATH; ?>php/fine_pay.php?id=<?php echo $row['fine_id']; ?>">
                                        Pay Fine</a>
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php } } ?>
                    </tbody>
                </table>

                <!--end: Datatable -->



                <!--end::Modal-->
            </div>
        </div>
    </div>





<?php include "includes/footer.php"; ?>