<?php


include "includes/nav.php";

?>
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        All Fines
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                    <tr>
                        <th>Book Name</th>
                        <th>Date Requested</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php


                     //$query = "SELECT * FROM fines";
                    $query = "SELECT fines.fine_amnt, borrow.borrow_id, users.username FROM fines, borrow, users WHERE fines.borrow_id = borrow.borrow_id";



                    $result = $db->query($query);

                    /* associative array */
                    if ($result->num_rows > 0) {
                        while ($row = mysqli_fetch_assoc($result)) {

                            ?>

                            <tr>
                                <td><?php echo $row['username']; ?></td>
                                <td><?php echo $row['fine_amnt']; ?></td>



                                <td><span class="dropdown">
                            <a href="<?php echo USER_PATH; ?>php/book_delete.php?id=<?php echo $row['book_id']; ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                              <i class="la la-trash"></i>
                            </a>

                        </span>
                                </td>


                            </tr>
                        <?php }
                    } ?>

                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>


<?php include("includes/footer.php"); ?>