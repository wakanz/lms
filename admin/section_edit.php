<?php

include "includes/nav.php";

?>


<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-8" style="margin: 0 auto;">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Edit Section
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" method="post" action="<?php echo ADMIN_PATH; ?>php/section_edit.php">
                    <div class="kt-portlet__body">
                        <tbody>
                        <?php


                        $query = "SELECT * FROM sections";
                        $result = $db->query($query);

                        /* associative array */
                        if ($result->num_rows > 0) {
                        while ($row = mysqli_fetch_assoc($result)) {

                        ?>

                        <div class="form-group row">

                            <input class="form-control" type="hidden" value="<?php echo $row['section_id']; ?>" name="section_id">

                        </div>
                        <div class="form-group row">
                            <label for="book_name" class="col-2 col-form-label">Section Name</label>
                            <div class="col-10">
                                <input class="form-control" type="text" value="<?php echo $row['section_name']; ?>" placeholder="Enter Author Name" name="name">
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-2">
                                </div>
                                <div class="col-10">
                                    <button name="submit" class="btn btn-success">Submit</button>

                                </div>
                            </div>
                        </div>
                        <?php } } ?>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<?php

include "includes/footer.php";

?>

